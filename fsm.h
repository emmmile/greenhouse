#ifndef FSM_H
#define FSM_H 1

#include <stdio.h>
#include <ctype.h>

// http://www.drdobbs.com/who-moved-my-state/184401643


// Event base class
class event {
};


// Finite State Machine base class
template<class E = event>
class fsm {
public:
  typedef void (fsm<E>::*state)(const E&);

  fsm ( state init ) {
    __state = init;
  }

  void initialize ( const E& e ) {
    (this ->* __state) ( e );
  }

  void dispatch ( const E& e ) {
    (this ->* ((fsm::state)__state)) ( e );
  }

  void transition ( state target ) {
    __state = target;
  }
protected:
  state __state; // the current state
};


typedef fsm<>::state state;


#endif









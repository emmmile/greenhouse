#ifndef KEYPAD_H
#define KEYPAD_H

#include "fsm.h"


class keyboard : public fsm<> {
  int                 __pin;
  int                 __reading;
  static const int    __accuracy = 60;

  // timers and intervals in milliseconds
  unsigned long       __timer;
  static const int    __press_interval = 500;
  static const int    __debounce_interval = 5;

  // holds the key currently pressed (or NO_KEY)
  int                 __last;


  // return true if the current analog reading match the i-th key
  inline bool is_key ( int analog_value, int i ) {
    return ( analog_value > __values[i] - __accuracy ) &&
           ( analog_value < __values[i] + __accuracy );
  }


  inline int detect_key ( int analog_value ) {
    for ( int i = CANCEL; i != NO_KEY; ++i )
      if ( is_key( analog_value, i ) ) return i;

    return NO_KEY;
  }

public:
  static const int    __values[];
  static const int    __keys = 4;

  enum value { CANCEL = 0, LEFT = 1, RIGHT = 2, OK = 3, NO_KEY = __keys };

  keyboard ( int pin )
    : fsm( (state) &keyboard::initial ), __pin( pin ), __timer( 0 ), __last( __keys ) {
  }

  int check ( ) {
    event read;
    dispatch( read );
    return __last;
  }

  // initial state, nothing is pressed
  void initial ( const event& ) {
    __reading = analogRead( __pin );
    __last = NO_KEY;
    __timer = millis();                             // start timer for the debouncing

    if ( detect_key( __reading ) != NO_KEY )        // debounce only if something meaningful seem to be pressed
      transition( (state) &keyboard::debounce );
  }

  // something could be pressed, but I need to debounce to be sure
  void debounce ( const event& ) {
    if ( millis() - __timer < __debounce_interval ) // before the debounce interval I do nothing
      return;

    int second = analogRead( __pin );               // I do a second reading

    if ( abs( __reading - second ) > __accuracy ) { // and if it is not close to the previous, I go back to initial state
      transition( (state) &keyboard::initial );
      return;
    }

    __reading = (__reading + second) / 2;           // I use the averege between the two readings
    __last = detect_key( __reading );               // the pressed key
    __timer = millis();                             // set the timer for the pressed interval
    transition( (state) &keyboard::pressed );
  }

  // something is currently pressed, waiting for release
  void pressed ( const event& ) {
    int current = analogRead( __pin );
    // I don't want more events to be generated
    __last = NO_KEY;
    // I don't care which key is still pressed, I only handle single pressures.
    // So if the key is released or the timer is expired I go back to normal state
    if ( detect_key( current ) == NO_KEY || ( millis() - __timer ) >= __press_interval )
      transition( (state) &keyboard::initial );
  }
};


const int keyboard::__values[keyboard::__keys] = { 200, 390, 555, 715 };

#endif // KEYPAD_H

# Smart Greenhouse project

This is basically a (very very) small operating system for Arduino. It handles some peripherals, some of them "standard" like an Hitachi 44780 LCD display, an LM35 temperature sensor.. Others instead are specific for my project and I built them. For the example the keyboard is 4 key one-analog-wire, resistor based keyboard ([where I got inspiration from](http://electronics.stackexchange.com/questions/18698/multiple-buttons-on-1-analog-vs-one-digital-per-buttonpower-consumption)). 

The program is still not perfect but the structure is more or less neat. There is class (actually called `base`) that implement all the low level features, for example given a set of sensors, update them every 250ms (with update I mean reading the value from the sensor). Or for example update what is written on the screen one time every second.

The program that runs on this small system is a very simple UI with a menu style. I know that there are many ready to use implementation of this, but I think this one is far more simple (and small in terms of executable). Items can be added to the UI through the `drawable` interface, and then they can be used through the keyboard connected to the Arduino.

### Strings

Finally I wanted also to redefine a string class (`stringbuffer`), very similar in concept to the stl one, but with the big difference that the length of the string is decided at compile time; so it is basically an array of chars with the interface of the `std::string` (work in progress..). I compiled this implementation on a Intel i3 and actually it outperforms the `std::string` for short sequences, in terms of performance on basic operations (concatenation for example), because does not use any kind of dynamic memory.
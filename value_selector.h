#ifndef VALUE_SELECTOR_H
#define VALUE_SELECTOR_H

#include "drawable.h"


class value_selector : public drawable {
  float*		__value;

  static constexpr float __increment = 1.0;
public:
  value_selector ( string id, float* v, drawable* p )
    : drawable( id, p ), __value( v ) {
  }

  float* value ( ) {
    return __value;
  }

  virtual string id ( ) const {
    if ( *__value == INFINITY || *__value == -INFINITY )
      return drawable::id();

    string out( __id );
    out.append( " (" );
    out.append( *__value, 2 );
    out.append( ")" );
    return out;
  }

  virtual void draw(display_event &e) {
    displaybuffer* s = e.buffer();

    (*s)[0].write( "  select value  " );
    (*s)[1].write( "  <   " );
    (*s)[1].append( *__value, 4 );
    (*s)[1].append( "   >  " );
  }

  virtual drawable* keys(keyboard_event &e) {
    drawable* out = this;
    char key = e.key();

    switch( key ) {
    case keyboard::OK:
      break;
    case keyboard::CANCEL:
      out = parent(); break;
    case keyboard::LEFT:
      *__value -= __increment; break;
    case keyboard::RIGHT:
      *__value += __increment; break;
    default:
      break;
    }

    return out;
  }
};


#endif // VALUE_SELECTOR_H

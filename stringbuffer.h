#ifndef STRING_H
#define STRING_H

#include <math.h>
#include <string.h>
#include <stdio.h>

#ifndef min
template <class T>
const T& min ( const T& a, const T& b ) {
	return !(b < a) ? a : b;     // or: return !comp(b,a)?a:b; for the comp version
}
#endif






template<unsigned int N>
class stackstring {
  char __data[N + 1];


  // computes the number of digits before (and including) the '.'
  long digits ( float x ) {
    long d = 1, integral = fabsf( x );
    for ( ; integral >= 10; integral /= 10, ++d );
    return ( x < 0 ? 1 : 0 ) + d + 1;
  }

  // integer pow, for saving space
  long pow10 ( int x ) {
    long out = 1;
    for ( ; x > 0; --x ) out *= 10;
    return out;
  }

public:
  void write ( const char& c ) {
    __data[0] = c;
    __data[1] = '\0';
  }

  void write ( const char* s ) {
    strncpy( __data, s, N + 1 );
    if ( strlen(s) >= N + 1 ) __data[N] = '\0';
  }

  void write ( const stackstring& s ) {
    write( s.str() );
  }

	void write ( const long int& i, const int width = N ) {
		itoa( i,__data, 10 );
		//snprintf( __data, N + 1, "%ld", i );
  }

	void write ( const int& i, const int width = N ) {
		itoa( i,__data, 10 );
		//snprintf( __data, N + 1, "%d", i );
  }

	void write ( float f, const int width = 4 ) {
    // sprintf does not support floating point number, neither width arguments like
    // ... "%*d", width, integer );
    // dtostrf solves partially the problem.
    // compute the decimal digits, that is width minus '.', sign and the integral part
    long decimaldigits = width - digits( f );
    // if f is too large it may not fit into width characters (even the integral part)
    // in this case cut the number to width bytes.
    if ( decimaldigits <= 0 ) {
			write( long( f ), width );
			//snprintf( __data, width + 1, "%ld", long( f ) );
      return;
    }

    //snprintf( __data, N + 1, "%.*f", decimaldigits, f );
    dtostrf( f, digits( f ), decimaldigits, __data );
  }

  stackstring ( ) {
    clear();
  }

  stackstring ( const char& c ) {
    write( c );
  }

  stackstring ( const char* s ) {
    write( s );
  }

  stackstring ( const long int& i ) {
    write( i );
  }

  stackstring ( const int& i ) {
    write( i );
  }

  stackstring ( float f, const unsigned long width = 4 ) {
    write( f, width );
  }

  stackstring ( double d, const unsigned long width = 4 ) {
    write( d, width );
  }

  template<unsigned int M>
  stackstring ( const stackstring<M>& s ) {
    write( s.str() );
  }

  void clear ( ) {
    memset( __data, '\0', N + 1 );
  }

  unsigned int length ( ) const {
    return strlen( __data );
  }

  const char* str () const {
    return __data;
  }

  template<unsigned int M = N + 1>
  stackstring<M> operator+ ( const char& c ) const {
    stackstring<1> tmp( c );
    return *this + tmp;
  }

  template<class T> // here N must be large enough to hold the value v
  stackstring operator+ ( const T& v ) const {
    stackstring tmp( v );
    return *this + tmp;
  }

  template<unsigned int M>
  stackstring<N+M> operator+ ( const stackstring<M>& s ) const {
    stackstring<N+M> out( *this );
    //printf( "'%s' '%s' => %d (%s)\n", __data, s.str(), N+M, out.str() );
    return out.append( s.str() );
  }

	stackstring& append ( const char* s ) {
		strncpy(  __data + length(), s, N + 1 - length() );
    return *this;
  }

  stackstring& append ( const stackstring& s ) {
    return append( s.str() );
  }

  stackstring& append ( long int i ) {
    stackstring tmp( i );
    return append( tmp );
  }

  stackstring& append ( float f, const unsigned long precision = 4 ) {
    stackstring tmp( f, precision );
    return append( tmp );
  }

  bool operator!= ( const stackstring& another ) const {
    return !( *this == another);
  }

  bool operator== ( const stackstring& another ) const {
    return strcmp( __data, another.__data ) == 0;
  }
};




template<unsigned int N,
         unsigned int M>
class stringbuffer {
  stackstring<N> __rows[M];
public:
  int rows ( ) const {
    return M;
  }

  int cols ( ) const {
    return N;
  }

  void clear ( ) {
    for ( int i = 0; i < M; ++i ) __rows[i].clear();
  }

  stackstring<N>& operator[] ( const int i ) {
    return __rows[i];
  }

  bool operator!= ( const stringbuffer& another ) const {
    return !( *this == another);
  }

  bool operator== ( const stringbuffer& another ) const {
    for ( int i = 0; i < M; ++i ) {
        if ( __rows[i] != another.__rows[i] ) return false;
    }

    return true;
  }
};



#endif // STRING_H

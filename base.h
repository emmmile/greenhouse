#ifndef BASE_H
#define BASE_H

#include "vector.h"
#include "fsm.h"
#include "sensors.h"
#include "config.h"
#include "ui.h"
#include "keyboard.h"
#include "switcher.h"

template <class T> void swap ( T& a, T& b ) {
  T c(a); a=b; b=c;
}


template<unsigned int N = COLS,
         unsigned int M = ROWS>
class base {
  LiquidCrystal*      __lcd;
  keyboard*           __keyboard;
  vector<sensor*>     __sensors;
  vector<switcher*>		__switches;

  drawable*           __ui;
  displaybuffer       __buffer[2]; // double buffering

  unsigned long       __keyboard_timer;
  unsigned long       __sensors_timer;
  unsigned long       __display_timer;

  static const int    __keyboard_tick = 10;
  static const int    __sensors_tick = 250;
	static const int    __display_tick = 1000;



public:
  base ( )
    : __keyboard_timer( 0 ), __sensors_timer( 0 ), __display_timer( 0 ) {
  }

  base ( LiquidCrystal* l, keyboard* k, drawable* u ) : __lcd( l ), __keyboard( k ), __ui( u ) {
  }

  void init ( ) {
    __lcd->begin( N, M );
  }

  void add_sensor ( sensor* a ) {
    __sensors.push_back( a );
    a->init();
  }

  void add_switch ( switcher* s ) {
    __switches.push_back( s );
    s->init();
  }

  void set_keyboard ( keyboard* k ) {
    __keyboard = k;
  }

  void set_lcd ( LiquidCrystal* l ) {
    __lcd = l;
  }

  void set_ui ( drawable* u ) {
    __ui = u;
  }

  void display ( ) {
    // create and dispatch display_event for the ui
    display_event e( __buffer + 1);
    // this draws into the buffer
    __ui->draw( e );


    // if the buffer has changed, then draws
    if ( __buffer[0] != __buffer[1] ) {
      swap( __buffer[0], __buffer[1] );

      __lcd->clear();
      for ( int i = 0; i < M; ++i ) {
        __lcd->setCursor( 0, i );
        __lcd->print( __buffer[0][i].str() );
      }
    }
  }

  void keys ( ) {
    int key = __keyboard->check();
    if ( key == keyboard::NO_KEY ) return;

    // create and dispatch keyboard_event for the ui
    keyboard_event e( key );
    __ui = __ui->keys( e );
  }

  void measure ( ) {
    // create and dispatch an event for me, so I directly do my stuff
    for ( int i = 0; i < __sensors.size(); ++i )
      __sensors[i]->measure();

    for ( int i = 0; i < __switches.size(); ++i )
      __switches[i]->check();
  }

  // some timers implementation
  void mainloop ( ) {
    unsigned long now = millis();
    if ( __keyboard_tick + __keyboard_timer <= now ) {
      __keyboard_timer = now;
      keys( );
    }

    if ( __sensors_tick + __sensors_timer <= now ) {
      __sensors_timer = now;
      measure();
    }

    if ( __display_tick + __display_timer <= now ) {
      __display_timer = now;
      display();
    }

		delay( 1 ); // small delay for idling a bit the device
  }
};


#endif // BASE_H

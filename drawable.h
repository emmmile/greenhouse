#ifndef VIEWABLE_H
#define VIEWABLE_H


#include "stringbuffer.h"
#include "config.h"



class display_event {
  displaybuffer* __buffer;
  // add a flag for knowing if buffer has been modified?
public:
  display_event( displaybuffer* buffer ) : __buffer( buffer ) { }
  displaybuffer* buffer ( ) { return __buffer; }
};



class keyboard_event {
  char __key;
public:
  keyboard_event( char key ) : __key( key ) { }
  char key ( ) { return __key; }
};


class drawable {
protected:
  string     __id;
  drawable*   __parent;
public:

  drawable( string id )
    : __id( id ), __parent( NULL ) {
  }

  drawable( string id, drawable* p )
    : __id( id ), __parent( p ) {
  }

  inline void set_parent( drawable* p ) {
    __parent = p;
  }

  inline drawable* parent ( ) {
    return __parent ? __parent : this;
  }


  virtual string id ( ) const {
    return __id;
  }

  virtual void draw ( display_event& s ) {
  }

  virtual drawable* keys ( keyboard_event& key ) {
    return this;
  }
};


#endif // VIEWABLE_H

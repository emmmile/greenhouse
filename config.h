#ifndef CONFIG_H
#define CONFIG_H

#define COLS   16
#define ROWS   2
#define DEBUG  0

#include "stringbuffer.h"

typedef stringbuffer<COLS,ROWS> displaybuffer;
typedef stackstring<COLS> string;

#endif // CONFIG_H



#include <Arduino.h>
#include <LiquidCrystal.h>
#include <OneWire.h>
#include "sensors.h"
#include "base.h"
#include "ui.h"
#include "switcher.h"
#include "battery_tester.h"



// peripherals
keyboard        keys    ( 2 );
lm35            atemp   ( 0, 1 );
ds18s20         dtemp   ( 8 );
LiquidCrystal   lcd     ( 12, 11, 5, 4, 3, 2 );
switcher        sw      ( 13, &atemp );

battery_tester  bat     ( 4 );

// create the system
menu            mainmenu( "Main menu" );
minmax_sensor_viewer   aitem   ( "analog temp", &atemp, "temp" );
minmax_sensor_viewer   ditem   ( "digital temp", &dtemp, "temp" );
base<COLS,ROWS> system  ( &lcd, &keys, &mainmenu );
switcher_viewer fswitch ( "fan switcher", &sw );
battery_tester_viewer   bitem   ( "battery", &bat );

void setup ( ) {
#if DEBUG > 0
  Serial.begin( 9600 );       // for communications, debug
#endif
  analogReference(INTERNAL);  // set the internal reference voltage to 1.1V, we get more precision from the sensors

  system.init();
  system.add_sensor( &atemp );
  system.add_sensor( &dtemp );
  system.add_switch( &sw );
  system.add_sensor( &bat );


  mainmenu.add_item( &aitem );
  mainmenu.add_item( &ditem );
  mainmenu.add_item( &bitem );
  mainmenu.add_item( &fswitch );
}

void loop ( ) {
  system.mainloop( );
}

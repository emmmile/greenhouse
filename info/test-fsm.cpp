#include "fsm.h"



struct keyboard_event : public event {
  char code;
};



struct keyboard : public fsm {
public:
  enum { // signals used by the Keyboard FSM
    SHIFT_DEPRESSED_SIG,
    SHIFT_RELEASED_SIG,
    ANY_KEY_SIG
  };

  keyboard( )
    : fsm( (state) &keyboard::Keyboard_initial ) {

    //FsmInit((Fsm *)&k, 0);
    event e;
    initialize( e );
    //FsmCtor_(&me->super_, &Keyboard_initial);
  }

  void Keyboard_initial ( const keyboard_event& e ) {
    // ... initalization of Keyboard attributes
    printf("Keyboard initialized");
    transition( (state) &keyboard::Keyboard_default );
    //FsmTran_((Fsm *)me, &Keyboard_default);
  }


  // the states
  void Keyboard_default ( const keyboard_event& e ) {
    switch (e.sig) {
    case SHIFT_DEPRESSED_SIG:
       printf("default::SHIFT_DEPRESSED");
      transition( (state) &keyboard::Keyboard_shifted );
       //FsmTran_((Fsm *)me, &Keyboard_shifted);
       break;
    case ANY_KEY_SIG:
       printf("key %c", (char)tolower(e.code));
       break;
    }
  }

  void Keyboard_shifted( const keyboard_event& e ) {
    switch (e.sig) {
    case SHIFT_RELEASED_SIG:
       printf("shifted::SHIFT_RELEASED");
       //FsmTran_((Fsm *)me, &Keyboard_default);
       transition( (state) &keyboard::Keyboard_default );
       break;
    case ANY_KEY_SIG:
       printf("key %c", (char)toupper(e.code));
       break;
    }
  }
};



int main() {
  keyboard k;
  for (;;) {
     keyboard_event ke;
     printf("\nSignal<-");
     ke.code = getc(stdin);
     getc(stdin);
     switch (ke.code) {
       case '^': ke.sig = keyboard::SHIFT_DEPRESSED_SIG; break;
       case '6': ke.sig = keyboard::SHIFT_RELEASED_SIG; break;
       case '.': return 0;
       default:  ke.sig = keyboard::ANY_KEY_SIG; break;
     }
     k.dispatch( ke );
     //FsmDispatch((Fsm *)&k, (Event *)&ke);
  }


  return 0;
}


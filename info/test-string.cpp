
#include <iostream>
#define rando(n)		float(random()) * n / RAND_MAX

#define STD 0
#define RUNS 1000000


#if !STD

#include "stringbuffer.h"

int main ( ) {
	stackstring<16> out;
	srandom(0);

	for ( int i = 0; i < RUNS; ++i ) {
		stackstring<5> first( rando(10), 5 );
		stackstring<4> mid  ( rando(20), 4 );
		stackstring<5> last ( rando(30), 5 );
		out = first + ' ' + mid + ' ' + last;
	}


	std::cout << out.str() << std::endl;

	return 0;
}

#else

#include <iomanip>
#include <sstream>
using namespace std;

int main ( ) {
	string out;
	srandom(0);

	for ( int i = 0; i < RUNS; ++i ) {
		stringstream tmp;
		tmp << fixed
				<< setprecision(3) << rando(10) << ' '
				<< setprecision(1) << rando(20) << ' '
				<< setprecision(2) << rando(30);

		out = tmp.str();
	}


	std::cout << out << std::endl;

	return 0;
}
#endif

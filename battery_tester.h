#ifndef BATTERY_TESTER_H
#define BATTERY_TESTER_H

#include "sensors.h"


class battery_tester : public sensor {
  int __pin;
  float __mah;
  float __mwh;
  unsigned long __timer;
public:
  battery_tester ( int pin ) : __pin( pin ), __mah( 0 ), __mwh( 0 ), __timer( 0 ) {
  }

  void measure () {
    float vout = analogRead( __pin ) / 1024.0 * 1.1 / ( 2170.0 / 10270.0 ); // in V
    unsigned long newtime = millis();

    float dt = ( newtime - __timer ) / 3600.0 / 1000.0; // in hours
    __value = vout; // values of resistances

    float current = vout / 30.0 * 1000.0; // in mA
    __mah += dt * current; // value of discharge resistance
    __mwh += dt * current * vout;

    __timer = newtime;
  }

  void init () {
    reset();
    __timer = millis();
  }

  float mah() {
    return __mah;
  }

  float mwh() {
    return __mwh;
  }
};


class battery_tester_viewer : public drawable {
  battery_tester* __s;

public:
  battery_tester_viewer ( string name, battery_tester* s ) : drawable( name ), __s( s ) {
  }

  void draw ( display_event& e ) {
    displaybuffer* s = e.buffer();

    (*s)[0].write( "vbat mAh   mWh" );
    (*s)[1].write( __s->value()   , 4 ); (*s)[1].append( " " );
    (*s)[1].append( __s->mah(), 5 ); (*s)[1].append( " " );
    (*s)[1].append( __s->mwh(), 5 );
  }

  drawable* keys ( keyboard_event& e ) {
    drawable* out = this;
    char key = e.key();

    switch( key ) {
    case keyboard::CANCEL:
      out = parent(); break;
    case keyboard::OK:
      __s->reset(); break;
    case keyboard::LEFT:
    case keyboard::RIGHT:
    default:
      break;
    }

    return out;
  }
};




#endif // BATTERY_TESTER_H

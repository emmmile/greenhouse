#ifndef SENSORS_H
#define SENSORS_H  1


#include "drawable.h"
#include "config.h"
#include "keyboard.h"





class sensor {
protected:
  float __value;
  float __minvalue;
  float __maxvalue;

public:
  static constexpr float max_limit = +99.0;
  static constexpr float min_limit = -99.0;

  inline float value ( ) { return __value; }
  inline float minvalue ( ) { return __minvalue; }
  inline float maxvalue ( ) { return __maxvalue; }

  void check_values ( ) {
    if ( __value < __minvalue ) __minvalue = __value;
    if ( __value > __maxvalue ) __maxvalue = __value;
  }

  void reset ( ) {
    __minvalue = __maxvalue = __value;
  }

  virtual void measure () = 0;

  virtual void init () = 0;
};


class minmax_sensor_viewer : public drawable {
  sensor* __s;
  stackstring<4> __abbreviation;
public:
  minmax_sensor_viewer ( string name, sensor* s, stackstring<4> a )
    : drawable( name ), __s( s ), __abbreviation( a ) {
  }

  void draw ( display_event& e ) {
    displaybuffer* s = e.buffer();

    string row = string(" min  ") + __abbreviation + string("  max");
    (*s)[0].write( row.str() );
    (*s)[1].write( __s->minvalue(), 5 ); (*s)[1].append( " " );
    (*s)[1].append( __s->value()   , 4 ); (*s)[1].append( " " );
    (*s)[1].append( __s->maxvalue(), 5 );
  }

  drawable* keys ( keyboard_event& e ) {
    drawable* out = this;
    char key = e.key();

    switch( key ) {
    case keyboard::CANCEL:
      out = parent(); break;
    case keyboard::OK:
      __s->reset(); break;
    case keyboard::LEFT:
    case keyboard::RIGHT:
    default:
      break;
    }

    return out;
  }
};




// temperature reader for the schematics 7 in the LM35 datasheet (page 8).
// I use only one diode, so the voltage range coming from the sensor ranges from -Vd to Vref-Vd, where:
//      Vd    is the voltage drop of the diode, the lower the better (a schottky diode could be great).
//      Vref  is the internal reference voltage of arduino (remember to use it!), this gives a
//            decent precision in the A/D conversion.
// In practice the temperature range should go from about -20 to +80 C, if using a schottky diode
class lm35 : public sensor {
  int __high_pin;      // analog pin for high output
  int __low_pin;       // analog pin for low output (ground)
  int __stable;         // XXX for the first reading problem

  static const int alpha = 4;
  static constexpr float multiplier = 1.1 * 1000.0 / ( 1024.0 * 10.0 ); // 1.1 is vref

public:
  lm35( int high_pin, int low_pin )
    : __high_pin( high_pin ), __low_pin( low_pin ), __stable( 0 ) {
    }

  void init ( ) {
    __minvalue = max_limit;
    __maxvalue = min_limit;
    __value = ( analogRead( __high_pin ) - analogRead( __low_pin ) ) * multiplier;
  }

  bool stable ( ) {
    return __stable > alpha * alpha;
  }

  void measure ( ) {
    if ( !stable() ) __stable++;

    // computes the EWMA
    float newtemp = ( analogRead( __high_pin ) - analogRead( __low_pin ) ) * multiplier;
    __value = ( newtemp / alpha + (alpha - 1) * __value / alpha );

    if ( stable() ) check_values();
  }
};









class ds18s20 : public sensor {
  OneWire __ds;

  byte __addr[8];    // holds the unique identifier of the sensor, used in communications
  byte __data[9];    // holds the temperature

public:
  ds18s20 ( int pin )
    : __ds( pin ) {
    memset( __addr, 0, 8 );
    memset( __data, 0, 9 );
  }

  void init ( ) {
    if ( !__ds.search( __addr ) ) {
      //no more sensors on chain, reset search
      __ds.reset_search();
      return;
    }

#if DEBUG > 0
    if ( OneWire::crc8( addr, 7 ) != addr[7] ) {
      Serial.println("CRC is not valid!");
      return;
    }

    if ( /*addr[0] != 0x10 &&*/ addr[0] != 0x28 ) {
      Serial.print("Device is not recognized");
      return;
    }
#endif

    startConversion(); // start conversion for the first time
    __minvalue = max_limit; // range of the sensor
    __maxvalue = min_limit;
  }

  void startConversion ( ) {
    __ds.reset();
    __ds.select(__addr);
    __ds.write(0x44);
#if DEBUG > 0
    Serial.println( "Conversion request sent." );
#endif
  }

  void measure ( ) {
    // if last conversion is still running, I don't need to send startConversion
    if ( __ds.read_bit() == 0 ) {
#if DEBUG > 0
      Serial.println( "Conversion in progress." );
#endif
    } else {
#if DEBUG > 0
      Serial.println( "Conversion done." );
#endif
      byte present = __ds.reset();
      __ds.select(__addr);
      __ds.write(0xBE); // Read Scratchpad

      // after a read scratchpad command the sensor will send 9 bytes
      for ( int i = 0; i < 9; ++i )
        __data[i] = __ds.read();

      __ds.reset_search();

      byte MSB = __data[1];
      byte LSB = __data[0];

      // measurement is quite slow, I don't want to perform an EWMA
      float tempRead = ((MSB << 8) | LSB); //using two's compliment
      __value = tempRead / 16;

      check_values();
      startConversion();
    }
  }
};



#endif

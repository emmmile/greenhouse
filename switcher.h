#ifndef SWITCHER_H
#define SWITCHER_H

#include "drawable.h"
#include "ui.h"
#include "sensors.h"
#include "value_selector.h"




class switcher {
  int			__pin;
  sensor* __sensor;
  float		__sw_value;

public:

  static constexpr float default_sw_value = 20.0;

  switcher ( int p, sensor* s, float v = default_sw_value )
  : __pin( p ), __sensor( s ), __sw_value( v ) {
  }

  float* value_address ( ) {
    return &__sw_value;
  }

  void always_on ( ) {
    __sw_value = -INFINITY;
  }

  void always_off ( ) {
    __sw_value = INFINITY;
  }

  void init ( ) {
    pinMode( __pin, OUTPUT );
  }

  void check ( ) {
    if ( __sensor->value() > __sw_value )
      digitalWrite( __pin, HIGH );
    else
      digitalWrite( __pin, LOW );
  }
};








class switcher_viewer : public menu {
  switcher*				__sw;
  drawable				__on;
  drawable				__off;
  value_selector	__selector;
  int							__selected;
public:
  switcher_viewer ( string id, switcher* s )
  : menu( id ), __sw( s ), __on( "always on" ), __off( "always off" ),
	__selector("temp", s->value_address(), this), __selected( 2 ) {
    __items.push_back( &__on );
    __items.push_back( &__off );
    __items.push_back( &__selector );
  }

  virtual string format_row ( int index ) {
    string out = menu::format_row( index );

    char selected_postfix[] = { ' ', 200, '\0' };
    if ( index == __selected ) out.append( selected_postfix );

    return out;
  }

  virtual drawable* keys(keyboard_event &e) {
    drawable* out = this;

    switch( e.key() ) {
      case keyboard::OK:
        __selected = __position;
        if ( __position == 2 ) {
          *__selector.value() = __sw->default_sw_value;
          out = __selector.keys( e );
        }

        if ( __position == 0 )
          __sw->always_on();
        if ( __position == 1 )
          __sw->always_off();

        break;
      default:
        out = menu::keys( e );
        break;
    }

    return out;
  }
};

#endif // SWITCHER_H

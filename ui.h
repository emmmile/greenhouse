#ifndef UI_H
#define UI_H  1

#include "drawable.h"
#include "fsm.h"
#include "vector.h"
#include "keyboard.h"




// implementation of a menu-like UI
class menu : public drawable {
protected:
  vector<drawable*>     __items;
  int                   __position;

  inline int __back ( ) {
    return ( __position + __items.size() - 1 ) % __items.size();
  }

  inline int __next ( ) {
    return ( __position + __items.size() + 1 ) % __items.size();
  }
public:
  menu ( string id )
    : drawable( id ), __position( 0 ) {
  }

  void add_item ( drawable* item ) {
    __items.push_back( item );
    item->set_parent( this );
  }

  int size ( ) const {
    return __items.size();
  }

  virtual string format_row ( int index ) {
    string out;

    if ( index == __position ) {
      out.write( (char) 201 ); out.append( " " );
    } else {
      out.write( "  " );
    }

    out.append( __items[index]->id() );
    return out;
  }

  virtual void draw ( display_event& e ) {
    displaybuffer* s = e.buffer();

    (*s)[0] = format_row( __position );
    (*s)[1] = format_row( __next() );
  }

  virtual drawable* keys ( keyboard_event& e ) {
    drawable* out = this;
    char key = e.key();

    switch( key ) {
    case keyboard::CANCEL:
      out = parent(); break;
    case keyboard::LEFT:
      __position = __back(); break;
    case keyboard::RIGHT:
      __position = __next(); break;
      break;
    case keyboard::OK:
      out = __items[__position]; break;
    default:
      break;
    }

    return out;
  }
};


#endif
